package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Config struct {
	Debug   bool   `json:"debug"`
	Users 	[]string `json:"users"`
	Paths	[]string `json:"dirs"`
}

var (
	config *Config
)

const (
	configName = "config.json"
)

func checkErr(err error) {
	if err != nil {
		if config.Debug == true {
			fmt.Println(err)
		}
	}
}

func getConfig() *Config {
	config := new(Config)
	configFile, err := os.Open(configName)
	checkErr(err)
	defer configFile.Close()
	jsonDecode := json.NewDecoder(configFile)
	err = jsonDecode.Decode(&config)
	checkErr(err)
	return config
}

func checkDir(dir string, config *Config) {
	files, err := ioutil.ReadDir(dir)
	checkErr(err)
	for _, file := range files {
		if file.IsDir() {
			if config.Debug == true {
				fmt.Println("- folder", file.Name())
			}
			err = os.RemoveAll(dir + file.Name())
			checkErr(err)
		} else {
			if config.Debug == true {
				fmt.Println("- file", file.Name())
			}
			err = os.Remove(dir + file.Name())
			checkErr(err)
		}
	}
}

func main() {
	config = getConfig()
	cPath := "C:\\Users\\"
	for _, user := range config.Users {
		for _, path := range config.Paths {
			fullPath := cPath + user + path
			checkDir(fullPath, config)
		}
	}
}
