use std::fs;
use std::io;
use serde::Deserialize;
use std::io::Read;

#[derive(Deserialize)]
struct Data {
    debug: bool,
    users: Vec<String>,
    paths: Vec<String>,
}

fn read_config() -> io::Result<Data> {
    let mut file = fs::File::open("config.json")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let data: Data = serde_json::from_str(&contents).unwrap();
    Ok(data)
}

fn dir_clean(user: &String, path: &String, debug: &bool) -> io::Result<()> {
    let full_path: String = "C:\\Common\\".to_owned() + user + path;
    let files = fs::read_dir(full_path)?;
    for f in files {
        let file = f.unwrap().path();
        if file.is_dir() {
            if *debug == true {
                println!("- folder {}", file.to_str().unwrap());
            }
            fs::remove_dir_all(file)?;
        } else {
            if *debug == true {
                println!("- file {}", file.to_str().unwrap());
            }
            fs::remove_file(file)?;
        }
    }
    Ok(())
}

fn main() {
    let data = read_config().unwrap();
    for user in &data.users {
        for path in &data.paths {
            match dir_clean(user, path, &data.debug) {
                Ok(_) => { continue },
                Err(_) => { continue },
            }
        }
    }
}
